var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
/* const { deleteOne } = require('../../models/bicicleta');
const bicicleta = require('../../models/bicicleta') */;

describe('Testing Bicicleta', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const bd = mongoose.connection;
        bd.on('error', console.error.bind(console, 'Connectioon error'));
        bd.once('open', function(){
            console.log('Me are connection to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Creamos una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana",  [10.4, -73.2]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(10.4);
            expect(bici.ubicacion[1]).toEqual(-73.2);
        });
    });
    
    describe('Bicicleta.allBicis', () => {
        it('comienza vacia',(done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });
    
    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo: "bmx"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code); 
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "bmx"});
                Bicicleta.add(aBici, function(err){
                    if (err) console.log(err);

                    var aBici2 = Bicicleta({code: 2, color: "rojo", modelo: "urbana"});
                    Bicicleta.add(aBici2,function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targeBici){
                            expect(targeBici.code).toBe(aBici.code);
                            expect(targeBici.color).toBe(aBici.color);
                            expect(targeBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });
    
    describe('Bicicleta.removeByCode', () => {
        it('Debe Eliminar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "bmx"});
                Bicicleta.add(aBici, function(err){
                    if (err) console.log(err);
                    var aBici2 = Bicicleta({code: 2, color: "rojo", modelo: "urbana"});
                    Bicicleta.add(aBici2,function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.removeByCode(1, function(error, targeBici){
                            expect(targeBici.ok).toBe(1);
        
                            done();
                        });
                     
                    });
                });
            });
        });
    });
});
/* beforeEach( function(){ Bicicleta.allBicis = [] });

describe('Bicicletas.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicletas.allBicis', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'verde', 'bmx');
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe delvolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'rosada', 'bmx');
        var aBici2 = new Bicicleta(2, 'rojas', 'montaña');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targeBici = Bicicleta.findById(1);
        expect(targeBici.id).toBe(1);
        expect(targeBici.color).toBe(aBici.color);
        expect(targeBici.modelo).toBe(aBici.modelo);
    });
});

describe('Bicicleta.removeById', () => {
    it(' debe eliminar la bici con id 1 ', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'rosada', 'bmx');
        var aBici2 = new Bicicleta(2, 'rojas', 'montaña');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targeBici = Bicicleta.removeById(1);
        expect(targeBici.id).toBe(1);
    });
}); */

