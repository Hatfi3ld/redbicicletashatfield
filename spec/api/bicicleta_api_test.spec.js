var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
const { findById } = require('../../models/bicicleta');

var base_url = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const bd = mongoose.connection;
        bd.on('error', console.error.bind(console, 'Connectioon error'));
        bd.once('open', function(){
            console.log('Me are connection to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });
    describe('GET BICICLETAS /',() => {
        it('Status 200',(done) => {           
           request.get(base_url, function(error, response, body){
               var result = JSON.parse(body);
               expect(response.statusCode).toBe(200);
               expect(result.bicicletas.length).toBe(0);
               done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "azul", "modelo": "Urbana", "lat": 10, "lng": -73}';
            request.post({
                headers: headers,
                url:  base_url + '/create',
                body: aBici
            },function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("azul");
                expect(bici.ubicacion[0]).toBe(10);
                expect(bici.ubicacion[1]).toBe(-73);
                done();
            });
        });
    });

    describe('PUT BICICLETAS /update', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "marron", "modelo": "ciclismo", "lat": 10, "lng": -73}';
            request.put({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/update/'+10,
                body: aBici
            },function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("marron");
                expect(Bicicleta.findById(10).modelo).toBe("ciclismo");
                done();
            });
        });
    }); 
   describe('DELETE BICICLETAS /delete', () => {
        it('status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "id": 10, "color": "azul", "modelo": "Urbana", "lat": 10, "lng": -73}';
            request.delete({
                headers: headers,
                url: base_url + '/delete',
                body: aBici
            },function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                expect(bici.id).toBe(10);
                done();
            });
        });
    });
});