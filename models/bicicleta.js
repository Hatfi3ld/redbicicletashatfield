var mongose = require('mongoose');
var Schema = mongose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion:{
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

//creacion de instancia
bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

//
bicicletaSchema.methods.toString = function() {
    return 'code: '+ this.code +' | color: ' + this.color + ' | modelo: ' + this.modelo + ' | ubicacion: ' + this.ubicacion;   
};

//muestra todas las bicicletas
bicicletaSchema.statics.allBicis = function(cb) {
    return this.find({}, cb); 
};

//Agregar bicicleta
bicicletaSchema.statics.add = function(aBici, cb) {
    this.create(aBici, cb); 
};

//Buscar Bicicleta
bicicletaSchema.statics.findByCode = function(aCode, cb) {
    return this.findOne({code: aCode}, cb); 
};

//Eliminar Bicicleta
bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({code: aCode}, cb); 
};

module.exports = mongose.model('Bicicleta', bicicletaSchema);